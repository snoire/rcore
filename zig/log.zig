const std = @import("std");
const sbi = @import("sbi");
const builtin = @import("builtin");
const options = @import("options");
const DebugInfo = @import("rvfsg").DebugInfo;

fn write(_: void, string: []const u8) error{}!usize {
    return sbi.putString(string);
}

const Writer = std.io.Writer(void, error{}, write);

pub fn print(comptime format: []const u8, args: anytype) void {
    std.fmt.format(Writer{ .context = {} }, format, args) catch unreachable;
}

const color = struct {
    pub const red = "\x1b[31m";
    pub const green = "\x1b[32m";
    pub const yellow = "\x1b[33m";
    pub const grey = "\x1b[90m";
    pub const none = "\x1b[m";
};

pub fn logFn(
    comptime level: std.log.Level,
    comptime scope: @TypeOf(.EnumLiteral),
    comptime format: []const u8,
    args: anytype,
) void {
    const level_color = switch (level) {
        .err => color.red,
        .warn => color.yellow,
        .info => color.green,
        .debug => color.grey,
    };
    const scope_text = "(" ++ @tagName(scope) ++ "): ";
    const prefix = level_color ++ "[" ++ comptime level.asText() ++ "] " ++ color.none ++ scope_text;

    print(prefix ++ format ++ "\n", args);
}

const print_stack_trace = options.trace;
var panic_buffer: [if (print_stack_trace) 0x1000_000 else 0]u8 = undefined; // 16MB

pub fn panicFn(msg: []const u8, _: ?*std.builtin.StackTrace, return_addr: ?usize) noreturn {
    @branchHint(.cold);
    print(color.red ++ "KERNEL PANIC: {s}!\n" ++ color.none, .{msg});

    if (print_stack_trace) {
        var fba: std.heap.FixedBufferAllocator = .init(&panic_buffer);
        var debug_info = DebugInfo.init(fba.allocator(), .{}) catch |err| {
            print("panic: debug info err = {any}\n", .{err});
            unreachable;
        };
        defer debug_info.deinit();

        const first_ret_addr = return_addr orelse @returnAddress();
        debug_info.printStackTrace(Writer{ .context = {} }, first_ret_addr, @frameAddress()) catch |err| {
            print("panic: stacktrace err = {any}\n", .{err});
            unreachable;
        };
    } else if (builtin.mode == .Debug and !builtin.strip_debug_info) {
        print(color.grey ++ "(use '-Dtrace' to see stack trace)\n" ++ color.none, .{});
    }

    sbi.shutdown(.shutdown, .system_failure);
}
