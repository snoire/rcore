const std = @import("std");
const builtin = @import("builtin");
const trap = @import("trap.zig");
const task = @import("task.zig");
const loader = @import("loader.zig");
const log = std.log.scoped(.kernel);

extern const stext: u8;
extern const etext: u8;
extern const srodata: u8;
extern const erodata: u8;
extern const sdata: u8;
extern const edata: u8;
extern const sbss: u8;
extern const ebss: u8;

const STACK_SIZE = 4096 * 16;
export var rcore_stack: [STACK_SIZE]u8 align(16) linksection(".bss.stack") = undefined;

const start_code = std.fmt.comptimePrint(
    \\  la sp, rcore_stack
    \\  li a0, {}
    \\  add sp, sp, a0
    \\
    \\  call main
, .{STACK_SIZE});

export fn _start() linksection(".text.entry") callconv(.Naked) noreturn {
    asm volatile (start_code);
}

export fn main() noreturn {
    log.info("Hello, world!", .{});

    log.debug(".text   [{*}, {*})", .{ &stext, &etext });
    log.debug(".rodata [{*}, {*})", .{ &srodata, &erodata });
    log.debug(".data   [{*}, {*})", .{ &sdata, &edata });
    log.debug(".bss    [{*}, {*})", .{ &sbss, &ebss });

    const stack_lower_bound: [*]u8 = &rcore_stack;
    log.debug("stack:  {*} => {*}", .{ stack_lower_bound + rcore_stack.len, stack_lower_bound });

    loader.init();
    trap.init();
    task.init();
    task.manager.first();
}

pub const panic = @import("log.zig").panicFn;
pub const std_options: std.Options = .{
    .page_size_min = 4096,
    // Define logFn to override the std implementation
    .logFn = @import("log.zig").logFn,
    .log_level = switch (builtin.mode) {
        .Debug => .debug,
        else => .info,
    },
};
