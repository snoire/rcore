pub const system = @import("rcore.zig");
const std = @import("std");

const Writer = std.io.Writer(system.fd_t, error{}, write);

fn write(fd: system.fd_t, string: []const u8) error{}!usize {
    errdefer comptime unreachable;
    const nbyte = system.write(fd, string.ptr, string.len);
    if (nbyte < 0) unreachable;
    return @intCast(nbyte);
}

pub fn print(comptime format: []const u8, args: anytype) void {
    const stderr = Writer{ .context = 2 };
    stderr.print(format, args) catch return;
}

pub fn panic(msg: []const u8, _: ?*std.builtin.StackTrace, ret_addr: ?usize) noreturn {
    @branchHint(.cold);
    print(color.red ++ "PANIC: {s}!\n" ++ color.none, .{msg});

    if (!@import("builtin").strip_debug_info) {
        const first_ret_addr = ret_addr orelse @returnAddress();
        var it: std.debug.StackIterator = .init(first_ret_addr, null);

        print("Stack Trace:\n", .{});
        while (it.next()) |addr| {
            print(" 0x{x}\n", .{addr});
        }
    }

    @trap();
}

const color = struct {
    const red = "\x1b[31m";
    const green = "\x1b[32m";
    const yellow = "\x1b[33m";
    const none = "\x1b[m";
};

const root = @import("root");
fn start() linksection(".text.entry") callconv(.C) noreturn {
    const exit_code: i32 = switch (@typeInfo(@TypeOf(root.main)).@"fn".return_type.?) {
        void => v: {
            root.main();
            break :v 0;
        },
        noreturn, i32 => root.main(),
        else => @compileError("expected return type of main to be 'void', 'noreturn' or 'i32'"),
    };
    system.exit(exit_code);
}

comptime {
    if (@hasDecl(root, "main")) {
        @export(&start, .{ .name = "_start", .linkage = .strong });
    }
}
