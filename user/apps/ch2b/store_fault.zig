pub const os = @import("os");
export fn _start() linksection(".text.entry") noreturn {
    os.print("Into Test store_fault, we will insert an invalid store operation...\n", .{});
    os.print("Kernel should kill this application!\n", .{});
    @as(*allowzero volatile u8, @ptrFromInt(0x0)).* = 0;
    unreachable;
}
